#ifndef PPM_H
#define PPM_H
#include "Imagem.hpp"
#include <iostream>
#include <fstream>

using namespace std;

  class Ppm : public Imagem {
//Atributos
    public:
        struct Rgb{
        char r,g,b;
        Rgb(): r(0), g(0), b(0) {}
        Rgb(char _r,char _g, char _b) : r(_r), g(_g), b(_b){}
      };
      Rgb *pixel;
//funções
      Ppm();
      Rgb& operator [] (const int &i) { return pixel[i]; }
      void lerPpm(ifstream &file,Ppm *ppm);
      void escreverPpm(ofstream &file,Ppm *ppm);
      void filtroAzul(ofstream &file,Ppm *ppm);
      void filtroVerde(ofstream &file,Ppm *ppm);
      void filtroVermelho(ofstream &file,Ppm *ppm);
};

#endif
