#ifndef IMAGEM_H
#define IMAGEM_H
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class Imagem{

//Atributos
private:
    int largura;
    int altura;
    int valor_max_cor;
    string numero_magico;
    char caminho_imagem[256];
    int cor;

//Métodos
public:
//Set
    Imagem();
    void setLargura(ifstream &arquivo);
    void setAltura(ifstream &arquivo);
    void setValorMaxCor(ifstream &arquivo);
    void setNumeroMagico(ifstream &arquivo);
    void setCaminhoImagem();
    void setCabecalho(ifstream &file);
//Get
    int getValorMaxCor();
    string getNumeroMagico();
    char *getCaminhoDaImagem();
    int getAltura();
    int getLargura();
    int getCor();
};

#endif
