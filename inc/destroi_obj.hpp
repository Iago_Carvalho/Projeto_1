#ifndef FUNCOES_H
#define FUNCOES_H
#include <iostream>
#include "Imagem.hpp"

using namespace std;

//Função para deletar objetos e liberar memoria
void checkObjectAlloc(Imagem* pobj);
void deleteObjects(Imagem* pobj1, Imagem* pobj2, Imagem* pobj3, Imagem* pobj4, Imagem* pobj5);

#endif
