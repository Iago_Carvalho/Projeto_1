#ifndef PGM_H
#define PGM_H
#include "Imagem.hpp"
#include <iostream>
#include <fstream>


using namespace std;

class PGM : public Imagem{

//Atributos
private:
  char bitExtraido;
  char byteArquivo;
  char caracter;

//Métodos
public:
  PGM();
  void decifrarPGM(ifstream &arquivo);
};

#endif
