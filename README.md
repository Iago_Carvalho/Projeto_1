--------------------------------------------------------------------------------------------------
README
Orientação a Objetos 
Exercício de programação 1
26/04/2016
Iago Vasconcelos de Carvalho
---------------------------------------------------------------------------------------------------

**Recomenda-se a leitura no modo "raw".

---------------------------------------------------------------------------------------------------
I. INFORMAÇÕES GERAIS
---------------------------------------------------------------------------------------------------
  -O software foi criado usando o OS Ubuntu 14.04 LTS.
  -É de extrema importância que o usuário utilize uma distribuição linux para o uso correto
   desse software.
  -Recomenda-se a utilização de uma distribuição 64-bit para a prevenção de erros. 
---------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
II. COMO USAR O PROGRAMA
----------------------------------------------------------------------------------------------------
  -A pasta "img" contém as imagens pgm e ppm separadas para identificação.
  -O programa possui caminhos de imagem e filtros pré definidos.
  -Para a seleção do caminho da imagem e escolha do filtro o usuário deverá definir os mesmos
   no arquivo "main.cpp" na pasta "src".
  -O arquivo "main.cpp" também possui instruções e exemplos internos com comentários para 
   orientar o usuário.
  -A imagem pgm decifrada será gerada na pasta raiz do programa e imagem ppm será gerada no diretório
   " img/ppm ".
   
   CAMINHO DA IMAGEM PGM 
  ----------------------------------
  -Para escolher a imagem PGM a ser decifrada o usuário deverá digitar o caminho da mesma na linha
   *24 do arquivo "main.cpp" como no exemplo a seguir: " img/pgm/imagem2.pgm ".
  
   CAMINHO DA IMAGEM PPM  
  ----------------------------------
  -Para escolher a imagem PPM a ser decifrada o usuário deverá digitar o caminho da mesma na linha
   *45 do arquivo "main.cpp" como no exemplo a seguir: " img/ppm/mensagem2.ppm ".
 
  ESCOLHA DO FILTRO
  ----------------------------------
  -Para escolher o filtro da imagem PPM o usuário deverá digitar na linha *69 do arquivo "main.cpp"
   o filtro desejado como nos exemplos:
      " segredo->filtroVerde(fileOut,segredo) ".
      " segredo->filtroAzul(fileOut,segredo) ".
      " segredo->filtroVermelho(fileOut,segredo) ".
--------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
III. INTRUÇÕES PARA COMPILAÇÃO DO PROGRAMA
-------------------------------------------------------------------------------------------------------
  -Para compilar o código o usuário deve fazer o download do mesmo pela plataforma gitlab ou 
   utilizar o comando " git clone git@gitlab.com:Iago_Carvalho/Projeto_1.git ".
  -O usuário deve utilizar o comando " cd " e procurar pelo diretório com a pasta raiz do programa
   em seu computador.
  -Na pasta raiz do programa o usuário deve digitar o seguinte comando " make " e aguardar até
   a mensagem ser exibida na tela.
  -Para compilar o programa o usuário deve digitar o comando " make run ". 
--------------------------------------------------------------------------------------------------------