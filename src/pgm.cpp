#include "pgm.hpp"
#include <cstdlib>

using namespace std;

PGM::PGM(){
  byteArquivo = 0;
  bitExtraido = 0;
  caracter = 0;
}
//Arquivo com a mensagem decifrada
void PGM::decifrarPGM(ifstream &arquivo){
ofstream arquivo_saida;
arquivo_saida.open("mensagem_pgm.txt", fstream::out);
//Caso ocorra erro ao criar arquivo de saida
if(!arquivo_saida)
{
  cerr << "Não foi possivel criar o arquivo de saida" << endl;
  exit(1);
}

arquivo.seekg(arquivo.tellg() + 5001, arquivo.beg);
while(caracter != '#')
{
  for(int i = 0; i< 8; i++)
  {
    arquivo.get(byteArquivo);
    bitExtraido = (byteArquivo & 0x01);
    caracter = (caracter << 1) | (bitExtraido& 0x01);
  }
  cout << caracter;
  arquivo_saida << caracter;
}
arquivo_saida.close();
}
