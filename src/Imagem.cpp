#include "Imagem.hpp"
#include <iostream>
#include <fstream>
#include <limits>


using namespace std;

//Set
void Imagem::setAltura(ifstream &arquivo)
{
  arquivo >> altura;
}

void Imagem::setLargura(ifstream &arquivo)
{
  arquivo >> largura;
}

void Imagem::setNumeroMagico(ifstream &arquivo){

  string num;
while(arquivo >> num){

    if(num == "#")
    {
      arquivo.ignore(numeric_limits<char>::max(), '\n');
      break;
    }
      numero_magico = num;
}
}
void Imagem::setCaminhoImagem(){
   cout << "Digite o caminho da imagem: ";
   cin.get(caminho_imagem, 256);
 }

void Imagem::setValorMaxCor(ifstream &arquivo)
{
  arquivo >> valor_max_cor;
}

 //Get
int Imagem::getAltura()
{
  return altura;
}

int Imagem::getLargura()
{
  return largura;
}

string Imagem::getNumeroMagico()
{
  return numero_magico;
}

int Imagem::getValorMaxCor(){
  return valor_max_cor;
}
Imagem::Imagem(){
  caminho_imagem[256];
  numero_magico;
  altura = 0;
  largura = 0;
  cor = 0;
}

void Imagem::setCabecalho(ifstream &file){
 char c;
   file >> numero_magico;
   while(file.get(c)){
     if(c == '#'){
       file.ignore(numeric_limits<char>::max(),'\n');
       break;
     }
   }
  file >> largura;
  file >> altura;
  file >> cor;
}
int Imagem::getCor(){
  return cor;
}
