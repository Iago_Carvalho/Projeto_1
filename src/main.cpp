#include "Imagem.hpp"
#include "destroi_obj.hpp"
#include "pgm.hpp"
#include "ppm.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main(int argc, char ** argv){

//Ponteiros das imagens
PGM * lena = new PGM();
Ppm * segredo = new Ppm();
Ppm * mensagem1 = new Ppm();
Ppm * mensagem2 = new Ppm();
Ppm * mensagem3 = new Ppm();

//ABRE O ARQUIVO PGM
//DIGITE O CAMINHO DA IMAGEM
//EXEMPLO: "img/pgm/imagem3.pgm"
ifstream file;
file.open("img/pgm/imagem3.pgm", ifstream::in | ifstream::binary);

if(!file){
  cerr << "O arquivo não pode ser aberto." << endl;
}

checkObjectAlloc(lena);
checkObjectAlloc(segredo);

lena->setNumeroMagico(file);
lena->setAltura(file);
lena->setLargura(file);
lena->setValorMaxCor(file);
lena->decifrarPGM(file); cout << endl;
//Fecha o arquivo PGM
file.close();


//ABRE O ARQUIVO PPM
//DIGITE O CAMINHO DA IMAGEM
//EXEMPLO: "img/ppm/mensagem2.ppm"
ifstream fileIn ("img/ppm/mensagem3.ppm",ifstream::in | ifstream::binary);
if(!fileIn){
  cerr << "Não foi possível abrir o arquivo" << endl;
  return 0;
 }

 segredo->setCabecalho(fileIn);
 cout << segredo->getNumeroMagico() << endl;
 cout << segredo->getLargura() << endl;
 cout << segredo->getAltura() << endl;
 cout << segredo->getCor() << endl;

 segredo->lerPpm(fileIn,segredo);

 ofstream fileOut("img/ppm/segredo_decifrado.ppm",ofstream::out | ofstream::binary);
 if(!fileOut){
   cerr << "Não foi possível abrir o arquivo" << endl;
   return 0;
  }

//ESCOLHA DO FILTRO
//EXEMPLO: segredo->filtroVerde(fileOut,segredo) para mensagem1.
//         segredo->filtroAzul(fileOut,segredo) para mensagem2.
//         segredo->filtroVermelho(fileOut,segredo) para mensagem3.
 segredo->filtroVermelho(fileOut,segredo);

//Fecha o arquivo
 fileIn.close();
 fileOut.close();


//chama a função e destroi os objetos
deleteObjects(lena, segredo, mensagem1, mensagem2, mensagem3);

return 0;
}
