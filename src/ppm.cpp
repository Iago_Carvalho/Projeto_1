#include "ppm.hpp"
#include <iostream>
#include <fstream>

using namespace std;

Ppm::Ppm(){
//Ler e escrever ppm
}
void Ppm::lerPpm(ifstream &file,Ppm *ppm){
  ppm->pixel = new Ppm::Rgb[ppm->getLargura() * ppm->getAltura()];
  char pix[3];
  for(int i=0;i< ppm->getLargura() * ppm->getAltura();i++){
    file.read(reinterpret_cast<char *>(pix), 3);
    ppm->pixel[i].r = pix[0];
    ppm->pixel[i].g = pix[1];
    ppm->pixel[i].b = pix[2];
  }
}
void Ppm::escreverPpm(ofstream &file,Ppm *ppm){
  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255";
  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = ppm->pixel[i].r;
    g = ppm->pixel[i].g;
    b = ppm->pixel[i].b;
    file << r << g << b;
  }
}


//Filtros
void Ppm::filtroAzul(ofstream &file,Ppm *ppm){
  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255\n";
  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = 0;
    g = 0;
    b = 255-ppm->pixel[i].b;
    file << r << g << b;
  }
}
void Ppm::filtroVerde(ofstream &file,Ppm *ppm){
  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255\n";
  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = 0;
    g = 255-ppm->pixel[i].g;
    b = 0;
    file << r << g << b;
  }
}
void Ppm::filtroVermelho(ofstream &file,Ppm *ppm){
  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255\n";
  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = 255-ppm->pixel[i].r;
    g = 0;
    b = 0;
    file << r << g << b;
  }
}
